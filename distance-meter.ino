#define trigPin 12
#define echoPin 11

#define buzzer 8
 
void setup() {
  Serial.begin (9600);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  
  pinMode(buzzer, OUTPUT);
  delay(500);
}
 
void loop() {
  long distance = measureDistance();
  signalDistance(distance);
  displayDistance(distance);
}

int measureDistance() {
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  long time = pulseIn(echoPin, HIGH);
  return time / 58;
}

void signalDistance(long distance) {
  if (distance < 100) {
    digitalWrite(buzzer, HIGH);
    delay(100);
    digitalWrite(buzzer, LOW);
    delay(20 + distance * distance / 10.0);
  } else {
    delay(500);
  }
}

void displayDistance(long distance) {
  Serial.print(distance);
  Serial.println(" cm");
}
